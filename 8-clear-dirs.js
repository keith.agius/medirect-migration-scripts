"use strict";

const fs = require('fs');
const rimraf = require('rimraf');
const path = require('path');

fs.readdir('attachments', async(err, files) => {
    if (err) throw err;
    console.log(`Clearing attachments folder`);
    for (const f of files) {
        rimraf(`attachments\\${f}`, function() { console.log(`Deleted ${f}`); });
    };
});

fs.readdir('tickets', async(err, files) => {
    if (err) throw err;
    console.log(`Clearing tickets folder`);
    for (const f of files) {
        rimraf(`tickets\\${f}`, function() { console.log(`Deleted ${f}`); });
    };
});

fs.readdir('batched_tickets', async(err, files) => {
    if (err) throw err;
    console.log(`Clearing batched_tickets folder`);
    for (const f of files) {
        rimraf(`batched_tickets\\${f}`, function() { console.log(`Deleted ${f}`); });
    };
});

fs.readdir('created_tickets', async(err, files) => {
    if (err) throw err;
    console.log(`Clearing created_tickets folder`);
    for (const f of files) {
        rimraf(`created_tickets\\${f}`, function() { console.log(`Deleted ${f}`); });
    };
});

/*
fs.readdir('users', async(err, files) => {
    if (err) throw err;
    console.log(`Clearing users folder`);
    for (const f of files) {
        rimraf(`users\\${f}`, function() { console.log(`Deleted ${f}`); });
    };
});
/*
fs.readdir('log_files', async(err, files) => {
    if (err) throw err;
    console.log(`Clearing log_files folder`);
    for (const f of files) {
        rimraf(`log_files\\${f}`, function() { console.log(`Deleted ${f}`); });
    };
});
*/