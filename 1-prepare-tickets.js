/*

Split ticket CSVs into separate batches

*/

"use strict";

const csv = require('csv-parser');
const fs = require('fs');
const subdomain = 'medirectbank1587974674';
const rimraf = require('rimraf');

var log;

module.exports = {prepareTickets};

//prepareTickets();
async function prepareTickets() {
    return new Promise(async (resolve, reject)=>{
        var t = await getToday();
        initLogFiles(t); // Initialise log files with today's date

        writeLog(`Starting Process`);
        console.log(`Starting Process`);

        await tickets().then(async () => {
            await attachments().then(()=>{
                resolve();
            });
        });
    });
};

async function tickets() {
    return new Promise(async (resolve, reject)=>{
        // Check if dir exists, if not, create it
        var tdir = `tickets`;
        if (!fs.existsSync(tdir)) {
            fs.mkdirSync(tdir);
        }

        fs.readdir(tdir, async(err, files) => {
            if (err) throw err;
            
            var tdir = 'tickets';
            fs.readdir(tdir, async(err, files) => {
                if (err) throw err;
                console.log(`Clearing ${tdir} folder`);
                for (const f of files) {
                    rimraf(`${tdir}\\${f}`, function() { console.log(`Deleted ${f}`); });
                };
            });

            // For each file
            fs.readdir(`source_tickets`, async(err, tickets) => {
                for (const t of tickets) {
                    var file = t;
                    await generateTickets(file); // Parse CSV, save tickets to a JSON format
                }
                resolve();
            });
        });
    });
}

async function attachments() {
    return new Promise(async (resolve, reject)=>{
        // Check if dir exists, if not, create it
        var adir = `attachments`;
        if (!fs.existsSync(adir)) {
            fs.mkdirSync(adir);
        }

        fs.readdir(adir, async(err, files) => {
            if (err) throw err;

            var adir = 'attachments'
            fs.readdir(adir, async(err, files) => {
                if (err) throw err;
                console.log(`Clearing ${adir} folder`);
                for (const f of files) {
                    rimraf(`${adir}\\${f}`, function() { console.log(`Deleted ${f}`); });
                };
            });

            // For each file
            fs.readdir(`source_attachments`, async(err, attachments) => {
                for (const a of attachments) {
                    var file = a;
                    await generateFiles(file); // Decode, generate and save attachments
                }
                resolve();
            });
        });
    });
}

// Generate attachments from hex format within the attachments.txt file
async function generateFiles(file) {
    return new Promise((resolve, reject) => {
        fs.createReadStream(`source_attachments\\${file}`).pipe(csv({ separator: '|' })).on('data', async(row) => {
            // Check if dir exists, if not, create it
            var dir = `attachments/${row.MessageId}`;
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            // Hex decode the data
            await decode(row, dir);
        })
        .on('end', async() => {
            resolve();
        });
    });
}

// Read from CSV and save to JSON format
function generateTickets(file) {
    return new Promise((resolve, reject) => {
        writeLog(`Reading from ${file} ...`);
        console.log(`Reading from ${file} ...`);
        var arrayCount = 0, rowCount=0;
        var tickets = [];
        var currentComment;

        fs.createReadStream(`source_tickets\\${file}`) // Create read stream
            .pipe(csv({ separator: '|' })) // Pipe into csv object
            .on('data', async(row) => { // Fire 'data' event each time a new row is processed
                try {
                    // Store the comment's attachments as an array within the comment itself
                    var commentAttachments = {};
                    commentAttachments = row.MessageId;
                    rowCount++;
                    // Some dates will not be pre-parsed into an array of 2 ... for some reason
                    if (row.CreatedDate.length > 2) {
                        // Format the date and remove milliseconds
                        row.CreatedDate = row.CreatedDate.toString().split(" "); // Format = dd/mm/yyyy hh:mm:ss (requires special formatting since defaulty is mm/dd ...)
                    }

                    var formatDate = toDate(row.CreatedDate[0]); // Date component
                    var formatTime = toTime(row.CreatedDate[1]); // Time component
                    var fullDate = `${formatDate} ${formatTime}`; // Concatenated date and time

                    var commentDate = new Date(Date.parse(fullDate));
                    commentDate = commentDate.toISOString().split('.')[0] + "+02:00"; // Time Zone formatting + Millisecond Handling

                    // Convert from hex to string
                    //var convertedContent = await hexToString(row.Content); // Discontinued after base64 conversion
                    var convertedContent = Buffer.from(row.Content, 'base64').toString();
                    convertedContent = convertedContent.replace(/\u0000/g, ""); // Remove Unicode encoding (null escape sequences)

                    // Format comment object
                    currentComment = {
                        'html_body': convertedContent,
                        'public': true,
                        'created_at': commentDate,
                        'author_id': row.CreatorId,
                        //'author_email': row.CreatorDisplayName.toLowerCase(),
                        'attachments': commentAttachments
                    };

                    // Format internal note object that contains comment details - Attachment details are added when uploading
                    var internalNote = {
                        'html_body': `Comment Creator ID: ${row.CreatorId}<br>Comment Creator Name: ${row.CreatorDisplayName}<br>Comment Creation Date: ${commentDate}`,
                        'public': false,
                        'created_at': commentDate,
                        'author_id': null,
                        'attachments': commentAttachments
                    }
                    
                    // Search for an entry in the ticketBatch that contains a ticket with the same ticketId (conversation ID)
                    var search = tickets.find(({ ticketId }) => ticketId === row.ConversationId);
                    if (search) { // Comment appended to existing ticket

                        var where = tickets.indexOf(search); // Get the location of the ticket within the array
                        tickets[where].comments.push(internalNote); // Add the current comment's original details as an internal note in case these are lost for an inexisting user
                        tickets[where].comments.push(currentComment); // Add the current (formatted) comment to the existing ticket's comments

                    } else { // New ticket

                        // Format ticket object:
                        tickets[arrayCount] = {
                            'subject': row.Subject, // Subject field for some reason was not being pulled correctly, so we are taking the first field in the file to be subject
                            'comments': [internalNote], // Internal note goes first
                            'created_at': commentDate,
                            'assignee_id': row.RecipientId,
                            //'assignee_name': row.RecipientDisplayName.toLowerCase(), // To make user mapping easier - Can be substituted for email
                            'requester_id': row.CreatorId,
                            //'requester_name': row.CreatorDisplayName.toLowerCase(), // To make user mapping easier - Can be substituted for email
                            'group_id': row.Branch,
                            'assignee_role': row.RecipientRole,
                            'requester_role': row.CreatorRole,
                            'ticketId': row.ConversationId,
                            'messageId': row.MessageId,
                            'message_type': row.MessageType,
                            'priority': 2,
                            'status': 5
                        };

                        tickets[arrayCount].comments.push(currentComment); // Formatted comment follows
                        arrayCount++; // Keep track of tickets within JSON object
                    }
                } catch (e) {
                    console.log('Error reading row', row, e);
                }
            })
            .on('end', async() => { // Fire 'end' event when all rows are processed
                writeLog(`${file} finished processing. ${rowCount} comments found. Generated ${arrayCount} tickets to file ...`);
                console.log(`${file} finished processing. ${rowCount} comments found. Generated ${arrayCount} tickets to file ...`);
                await saveTickets(file, tickets);
                resolve();
            });
    });
}

// Save the processed csv as a JSON object in txt format
function saveTickets(file, tickets) {
    return new Promise((resolve, reject) => {
        try {
            writeLog(`Saving tickets from ${file} to ${file.substring(0, file.length - 4)}.log\n`);
            console.log(`Saving tickets from ${file} to ${file.substring(0, file.length - 4)}.log\n`);
            fs.createWriteStream(`./tickets/${file.substring(0, file.length - 4)}.log`, { flags: 'a' }).write(JSON.stringify(tickets)); // Make sure the ticket folder exists!
            resolve();
        } catch (e) {
            writeLog(`Error saving tickets from ${file} to ${file.substring(0, file.length - 4)}.log\n`);
            console.log('Error writing tickets', e);
            reject();
        }
    });
}

// Hex decode the data (decoding happens in hexToFile)
function decode(row, dir) {
    return new Promise(async(resolve, reject) => {
        var file = [];
        file = row.FileName.split("\\");
        
        if (file.length > 1) {
            file = file[file.length - 1];
        } else {
            file = file[0];
        };

        file = file.replace(/[^a-zA-Z0-9. ]/g, '');

        fs.writeFile(`${dir}/${file}`, await hexToFile(row.Data), function(err, result) {
            if (err) {
                console.log('File conversion error', err);
                resolve();
            } else {
                console.log(`Generated ${dir}/${file}`);
                writeLog(`Generated ${dir}/${file}`);
                resolve();
            }
        });
    });
}

const toDate = (dateStr) => {
    const [day, month, year] = dateStr.split("/");
    return (`${month}/${day}/${year}`);
}

function toTime(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice(1); // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
}

// Write to log file with timestamp
async function writeLog(msg) {
    var t = await getToday();
    log.write(t + " - " + msg + "\n");
}

// Initialise log files
function initLogFiles(date) {
    log = fs.createWriteStream(`log_files/${date}_1-PREPARE-TICKETS-${subdomain}.log`, { flags: 'a' }); // Process log file
}

// Get today's date and time in an appropriate format for logging purposes
function getToday() {
    return new Promise((resolve, reject) => {
        var today = new Date();
        var d = String(today.getDate()).padStart(2, '0');
        var m = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var y = today.getFullYear();
        var h = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
        var min = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
        var s = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();
        var mer = today.getHours() > 12 ? "PM" : "AM";

        today = `${y}-${m}-${d}_${h}-${min}-${s}${mer}`;
        resolve(today);
    });
}

// Convert message content from hex to string
function hexToString(hex) {
    return new Promise((resolve, reject) => {
        var string = '';
        for (var i = 0; i < hex.length; i += 2) {
            string += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        }
        resolve(string);
    });
}

// Convert file data from hex to Uint8/Binary
function hexToFile(hex) {
    return new Promise((resolve, reject) => {
        // Clean hex
        hex = hex.toUpperCase();
        hex = hex.replace(/0x/gi, "");
        hex = hex.replace(/[^A-Fa-f0-9]/g, "");

        // Convert
        var binary = new Array();
        for (var i = 0; i < hex.length / 2; i++) {
            var h = hex.substr(i * 2, 2);
            binary[i] = parseInt(h, 16);
        }
        var byteArray = new Uint8Array(binary);
        resolve(byteArray);
    });
}