/*

Uploads tickets and tracks jobs for restarting in case of failed jobs

*/

'use strict';
const fs = require('fs');
const path = require('path');
const zd = require('./zendesk-lib.js');

// Set Zendesk credentials
//const username = 'keith.agius@imovo.com.mt/token';
const username = 'opzendeskapi@medirect.com.mt/token';
//const password = 'J44YNedG0GvZFP26PYGPjjxKleTpQLvOAa6QD5Wa'; // d3v-imovo token
const password = 'mwcMTvgaw6u2CeBL1VPi3efvO7SR5GGM1wUQcx0j'; // medirectbank1587974674 token
//const subdomain = 'd3v-imovo';
const subdomain =  'medirectbank1587974674';

// Set log file locations
var log, job, logFail;
//, createdTickets;
var jobs = [],
    batches = [];
//    ,ticketIds = [];
var progress = 0,
    total = 0,
    batch, i = 0,
    checker = 0;

module.exports = {main};

//main();
async function main() {
    return new Promise(async (resolve, reject) => {
        var t = await getToday();
        initLogFiles(t); // Initialise log files with today's date

        writeLog(`Starting upload process`);
        zd.setCredentials(username, password, subdomain);
        await trackBatch().then(()=>{
            resolve();
        });
    });
};

// Recursively keep track of which batched_tickets_n we are working in
function trackBatch() {
    return new Promise((resolve, reject) => {
        const dir = 'batched_tickets';
        fs.readdir(dir, async(err, folders) => {
            console.log(`${folders.length} total folder(s) found`);
            if (i < folders.length) { // Check if there is a batch currently working
                processBatch(`batched_tickets/${folders[i]}`); // Process the current folder
                i++;
            } else if (checker === 1 && i === folders.length) { // All batches finished and all failed jobs are saved
                writeLog(`All Batches Finished`);
                console.log('All Batches Finished');
                //createdTickets.write(JSON.stringify(ticketIds)); // Write the full list of IDs to file
                resolve();
            } else { // The script should never arrive here, if it did, I'd love to know how :)
                console.log('Waiting on previous batch to finish');
                setTimeout(() => {
                    trackBatch();
                }, 5000);
            }
        });
    });
}

// Loop through each file within the folder to upload its tickets
// Includes job tracking and fail handling
function processBatch(b) {
    batch = b;
    writeLog(`${batch} - Starting`);
    console.log(`${batch} - Starting`);
    fs.readdir(batch, async(err, files) => {
        total = files.length;
        progress = 0;
        for (var j = 1; j <= files.length; j++) { // For each file in the batch folder
            const currentBatch = `batched_${j}.log`;
            var data = JSON.parse(fs.readFileSync(`${batch}/${currentBatch}`, 'utf8')); // Get file data

            if (jobs.length <= 30) {
                if (data.tickets.length != 0) { // If data is found in the file
                    progress++;
                    writeLog(`Uploading ${currentBatch} \t_ ${progress}/${total}`);
                    printProgress(`Uploading ${currentBatch}`);
                    await uploadTickets(data, currentBatch).catch(error => { // Work on the tickets
                        console.log('4', error);
                        console.log(`${currentbatch}`);
                    });
                    await checkJobs(); // Check job & batch status
                } else {
                    progress++;
                    writeLog(`Skipping ${currentBatch} (EMPTY FILE) \t_ ${progress}/${total}`);
                    printProgress(`Skipping ${currentBatch} (EMPTY FILE)`);
                    await checkJobs(); // Check job & batch status
                }
            } else {
                setTimeout(async() => {
                    writeLog(`30+ jobs queued, waiting \t_ ${progress}/${total}`);
                    printProgress(`30+ jobs queued, waiting`);
                    await checkJobs(); // Check job & batch status
                }, 5000);
            }
        };
    });
}

// Checks job and batch statuses
async function checkJobs() {
    return new Promise(async(resolve, reject) => {
        if (jobs.length <= 30) {
            writeJob(`Checking ${jobs.length} jobs \t_ ${progress}/${total}`);
            printProgress(`Checking ${jobs.length} jobs`);
            checker = 0;
            var error = 0;
            let res = await zd.getManyJobsStatus(jobs).catch(error => {
                console.log('5', error);
                error = 1;
            });
            
            if (error === 0) {
                res = JSON.parse(res);            
            
                for (let x = 0; x < res.job_statuses.length; x++) {
                    let job = res.job_statuses[x];
                    writeJob(`${job.id} - Checking`);
                    if (job.status === 'completed') {
                        writeJob(`${job.id} - Success\n`);

                        var ticketIds = [];

                        for (var t = 0; t < job.results.length; t++) { // For each item in the result
                            ticketIds.push(job.results[t].id); // Save the IDs
                        }
                        
                        // Check if dir exists, if not, create it
                        var createdTicketsDir = `created_tickets`;
                        if (!fs.existsSync(createdTicketsDir)) {
                            fs.mkdirSync(createdTicketsDir);
                        }

                        var createdTickets = fs.createWriteStream(`${createdTicketsDir}/${job.id}.log`, { flags: 'a' }); // Created tickets log file
                        createdTickets.write(JSON.stringify(ticketIds));
                        createdTickets.end();

                        // Remove succesful job ID and file name from respective arrays
                        const index = jobs.indexOf(job.id);
                        if (index > -1) {
                            jobs.splice(index, 1);
                            batches.splice(index, 1);
                        }
                        checker = 1;

                    } else if (job.status === 'failed') { // Failed jobs are stored in a separate log file so that they can be re-uploaded at a later stage
                        const index = jobs.indexOf(job.id);
                        writeLog(`${job.id} || ${batch}/${batches[index]} - Fail (check FAILED_JOBS.log)`);
                        writeJob(`${job.id} || ${batch}/${batches[index]} - Fail (check FAILED_JOBS.log)\n`);
                        writeFail(`${job.id} || ${batch}/${batches[index]} - Fail`);
                        writeFail(`${JSON.stringify(job)}`);
                        console.log(`${job.id} || ${batch}/${batches[index]} - Fail (check FAILED_JOBS.log)`);

                        console.log(`\nBatch: ${batch}/${batches[index]}\nTickets ${job.progress} to ${job.total} created, getting external IDs`)
                        var failData = JSON.parse(fs.readFileSync(`${batch}/${batches[index]}`, 'utf8'));
                        var extIDs = [], failedTickets = {"tickets": []};
                        
                        var trackprogress = job.progress - 1, end = job.total - 1;
                        if (trackprogress < 0) { trackprogress = 0; }

                        for (var f = 0; f <= end; f++) {
                            if (f < trackprogress) {
                                extIDs.push(failData.tickets[f].external_id);
                            } else {
                                failedTickets.tickets.push(failData.tickets[f]);
                            }
                        }

                        // Check if dir exists, if not, create it
                        var extIdsDir = `created_ext_ids`;
                        if (!fs.existsSync(extIdsDir)) {
                            fs.mkdirSync(extIdsDir);
                        }

                        var createdExtIds = fs.createWriteStream(`${extIdsDir}/${job.id}.log`, { flags: 'a' }); // Created tickets log file
                        createdExtIds.write(JSON.stringify(extIDs));
                        createdExtIds.end();

                        // Check if dir exists, if not, create it
                        var failedTicketsDir = `failed_tickets`;
                        if (!fs.existsSync(failedTicketsDir)) {
                            fs.mkdirSync(failedTicketsDir);
                        }

                        var failedTickets = fs.createWriteStream(`${failedTicketsDir}/${job.id}.log`, { flags: 'a' }); // Created tickets log file
                        failedTickets.write(JSON.stringify(failedTickets));
                        failedTickets.end();

                        // Remove failed job ID and file name from respective arrays
                        if (index > -1) {
                            jobs.splice(index, 1);
                            batches.splice(index, 1);
                        }

                        checker = 1;
                    } else if (job.status === 'working') {
                        writeJob(`${job.id} - ${job.status} - ${job.progress}/${job.total} - ${job.message}\n`);
                    } else {
                        writeJob(`${job.id} - ${job.status} - ${job.message}\n`);
                    }
                };
            } else {
                console.log(`\nError encountered in job tracker ... Re-trying`);
            };
            if (progress === total) { // All files within the batch are processed
                if (jobs.length === 0) {
                    writeLog(`${batch} - Finished\n`);
                    console.log(`\n${batch} - Finished\n`);
                    await trackBatch(); // Current batch finished, move to next batch
                    resolve();
                } else { // Entire process has finished and is currently waiting on jobs, so recursively checkjobs
                    checkJobs();
                }

            } else {
                resolve(); // Move to next file within batch
            }
        } else if (jobs.length > 30) { // Recursively check job statuses until space is freed
            writeLog(`30+ jobs queued, waiting \t_ ${progress}/${total}`);
            printProgress(`30+ jobs queued, waiting`);
            setTimeout(async() => {
                await checkJobs();
            }, 5000);
        }
    });
}

// Uploads tickets
async function uploadTickets(t, b) {
    return new Promise((resolve, reject) => {
        zd.importManyTickets(t).then(data => {
            jobs.push(data.job_status.id); // Store new job id
            batches.push(b); // Store the job's batch name
            if (jobs.length > 5) {
                writeLog(`${jobs.length} jobs queued, slowing down by ${(1000 * (jobs.length - 5))/1000}s\t_ ${progress}/${total}`);
                printProgress(`${jobs.length} jobs queued, slowing down by ${(1000 * (jobs.length - 5))/1000}s`);
                setTimeout(() => {
                    resolve(); // Upload finished
                }, 1000 * (jobs.length - 5));
            } else {
                resolve(); // Upload finished
            }
        }, err => {
            reject(err);
        });
    });
}

// Write to upload log file with timestamp
async function writeLog(msg) {
    var t = await getToday();
    log.write(t + " - " + msg + "\n");
}

// Write to job log file with timestamp
async function writeJob(msg) {
    var t = await getToday();
    job.write(t + " - " + msg + "\n");
}

// Write to failed job log file with timestamp
async function writeFail(msg) {
    var t = await getToday();
    logFail.write(t + " - " + msg + "\n");
}

function printProgress(msg) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(`${msg} \t\t_ ${progress}/${total} (${batch})`);
}

// Initialise log files
function initLogFiles(date) {
    log = fs.createWriteStream(`log_files/${date}_3-UPLOAD-TICKETS-${subdomain}.log`, { flags: 'a' }); // Upload log file
    job = fs.createWriteStream(`log_files/${date}_3-UPLOAD-TICKETS-${subdomain}_JOBS.log`, { flags: 'a' }); // Job log file
    logFail = fs.createWriteStream(`log_files/${date}_3-UPLOAD-TICKETS-${subdomain}_FAILED_JOBS.log`, { flags: 'a' }); // Failed jobs log file
    //createdTickets = fs.createWriteStream(`log_files/${date}_3-UPLOAD-TICKETS-${subdomain}_TICKETS.log`, { flags: 'a' }); // Created tickets log file
}

// Get today's date and time
function getToday() {
    return new Promise((resolve, reject) => {
        var today = new Date();
        var d = String(today.getDate()).padStart(2, '0');
        var m = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var y = today.getFullYear();
        var h = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
        var min = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
        var s = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();
        var mer = today.getHours() > 12 ? "PM" : "AM";

        today = `${y}-${m}-${d}_${h}-${min}-${s}${mer}`;
        resolve(today);
    });
}