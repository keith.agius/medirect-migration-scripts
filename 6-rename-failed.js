'use strict';

const fs = require('fs');
var dir = 'failed_tickets';

// Check if dir exists, if not, create it
var dir_nest = `${dir}/batched_tickets_0`;
if (!fs.existsSync(dir_nest)) {
    console.log(`Creating ${dir_nest}`);
    fs.mkdirSync(dir_nest);
}

fs.readdir(dir, async(err, files) => {
    if (files.length > 0) {
        for(var i = 0; i<files.length; i++) {
            fs.rename(`${dir}/${files[i]}`, `${dir_nest}/batched_${i+1}.log`,()=>{});
        }
        console.log(`Finished renaming - New files found in ${dir_nest}`);
        console.log(`Files are ready for use in batched_tickets folder`);
    } else {
        console.log(`No files found`);
    }
});