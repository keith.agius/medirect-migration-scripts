/*

Maps tickets into upload-ready format
Upload comment attachments for use

*/

'use strict';
const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');
const zd = require('./zendesk-lib');
const request = require("request");
const rimraf = require('rimraf');
var readline = require('readline');
const { resolve } = require('path');

// Set Zendesk credentials
//const username = 'keith.agius@imovo.com.mt/token';
const username = 'opzendeskapi@medirect.com.mt/token';
//const password = 'J44YNedG0GvZFP26PYGPjjxKleTpQLvOAa6QD5Wa'; // d3v-imovo token
const password = 'M3K2F5wwIf5jZH08UhSsJ03kA68DfOVpptsA57iU'; // medirectbank1587974674 token
//const subdomain = 'd3v-imovo';
const subdomain =  'medirectbank1587974674';

// Set log file locations
var log, uploadLog;

var fileCount, dir, ticketsPerFile = 100;
var users = [];
//var default_user = { "id": "361806079479" }; // Keith Agius d3v-imovo
var default_user = {"id": 369888112099, "email": "opzendeskapi@medirect.com.mt"}; // OPZendeskAPI Medirect
var uploadError = 0;

module.exports = {mapTickets};

//mapTickets();
async function mapTickets() {
    return new Promise(async(resolve, reject) => {    
        //await getUsers(); // Get entire list of users

        // Read from users.csv
        await usersFromCsv();
        var userResult = users;

    //  var fieldResult = await getFields(); // Get entire list of ticket fields
    //  var ticketTypeField = await findField(fieldResult, 'Ticket Type'); // Get Ticket Type field id and name

        var t = await getToday();
        initLogFiles(t); // Initialise log files with today's date and time

        writeLog('Starting mapping process');
        console.log('Starting mapping process');
        zd.setCredentials(username, password, subdomain);

        await clearBatchedTickets();

        fs.readdir('tickets', async(err, files) => {
            fileCount = files.length;
            writeLog(`${fileCount} file(s) detected.`);
            console.log(`${fileCount} file(s) detected.`);

            // For each prepared file in the tickets folder, we are creating a folder containing batches of 30 maximum tickets each
            for (var n = 0; n < fileCount; n++) {
                var file = `tickets/${files[n]}`;
                writeLog(`Starting ${file}`);
                console.log(`Starting ${file}`);
                var data = JSON.parse(fs.readFileSync(`${file}`, 'utf8')); // Tickets are stored in 'data'
                var batchTrack = 0, batchEnd = ticketsPerFile - 1;

                // Check if dir exists, if not, create it
                dir = `batched_tickets/batched_tickets_${n}`;
                if (!fs.existsSync(dir)) {
                    console.log(`Creating ${dir}`);
                    fs.mkdirSync(dir);
                }

                // Calculate number of files
                var filesToCreate = Math.ceil(data.length / ticketsPerFile);
                
                //fs.readdir(dir, async(err, files) => {
                //    if (err) throw err;

                    writeLog(`Creating ${filesToCreate} batch files within ${dir}`);
                    console.log(`Creating ${filesToCreate} batch files within ${dir}`);
                    writeLog(`${data.length} total tickets || ${ticketsPerFile} tickets per file`);
                    console.log(`${data.length} total tickets || ${ticketsPerFile} tickets per file`);

                    // Populate each batch file with a maximum of 'ticketsPerFile' tickets
                    for (var j = 1; j < filesToCreate + 1; j++) {
                        const batchFile = fs.createWriteStream(`${dir}/batched_${j}.log`, { flags: 'a' });
                        var ticketBatch = { 'tickets': [] };

                        if (batchEnd >= data.length) {
                            // Ensure that the max ticket count is respected
                            batchEnd = data.length-1;
                        };

                        //writeLog(`Writing tickets ${batchTrack} to ${batchEnd} in ${dir}/batched_${j}.log`);
                        //console.log(`Writing tickets ${batchTrack} to ${batchEnd} in ${dir}/batched_${j}.log`);
                        process.stdout.write(`Working ... ${batchTrack} to ${batchEnd}`);
                        process.stdout.cursorTo(0);

                        for (var x = batchTrack; x <= batchEnd; x++) {
                            var currentTicket = data[x];

                            // Initialise the current ticket's comments objects
                            var currentComments = [];

                            if (data[x] != undefined) { // data[x] has valid data
                                for (var c = 0; c < data[x].comments.length; c++) { // For each comment
                                    var tokens = [],
                                        fileNames = [];
                                    var currentAttachments = currentTicket.comments[c].attachments;

                                    // Author lookup
                                    // Author can be an agent or an end-user
                                    if (currentTicket.comments[c].public === true) {
                                        var author = await findUser(userResult, currentTicket.comments[c].author_id);
                                        if (author.id === 1) { // No result found
                                            author = default_user;
                                        }
                                    } else { // Internal note (public is false)
                                        author = default_user;
                                    }

                                    if (currentTicket.comments[c].public === false) {
                                        // For an internal note, we are appending the attachment name to the details
                                        // Tokens are appended regardless since it is always empty for internal notes
                                        // The toggle is set to internal in the uploads function so that the attachment names are returned
                                        await upload(currentAttachments, currentTicket.ticketId, x, c, tokens, fileNames, 'internal');
                                        currentComments[c] = {
                                                'html_body': currentTicket.comments[c].html_body + `<br>Attachment(s): ${fileNames.length != 0 ? fileNames : `None`}`,
                                                'public':       currentTicket.comments[c].public,
                                                'created_at':   currentTicket.comments[c].created_at,
                                                'author_id':    author.id,
                                                'uploads':      tokens
                                            };

                                    } else {
                                        // Get the tokens from the upload and append them to the ticket comment's uploads section
                                        await upload(currentAttachments, currentTicket.messageId, x, c, tokens, fileNames, 'public');
                                        currentComments[c] = {
                                            'html_body':    currentTicket.comments[c].html_body,
                                            'public':       currentTicket.comments[c].public,
                                            'created_at':   currentTicket.comments[c].created_at,
                                            'author_id':    author.id,
                                            'uploads':      tokens
                                        };
                                        
                                    }
                                };

                                // Ticket mapping goes here with the updated comments array that includes 'uploads': <tokens>
                                var groupId, priority, status;

                                // Assignee lookup
                                // Same as author
                                var assignee = await findUser(userResult, currentTicket.assignee_id);
                                if (assignee.id === 1) { // No result found
                                    assignee = default_user;
                                }
                            
                                // Requester lookup
                                // Same as author
                                var requester = await findUser(userResult, currentTicket.requester_id);
                                if (requester.id === 1) { // No result found
                                    requester = default_user;
                                }

                                // Switch requester and assignee users in case the assignee is marked as the end-user
                                if (currentTicket.assignee_role === 'end-user') {
                                    var temp = requester;
                                    requester = assignee;
                                    assignee = temp;
                                }

                                /*
                                // d3v-imovo
                                switch (currentTicket.group_id) {
                                    default:
                                    case 1: groupId = 360001476600;    break; // Malta
                                    case 2: groupId = 360001476999;    break; // Belgium
                                }
                                */
                                
                                // medirect
                                switch (currentTicket.group_id) {
                                    default:
                                    case 1: groupId = 360003032220;    break; // Malta
                                    case 2: groupId = 360003234520;    break; // Belgium
                                }

                                switch (currentTicket.priority) {
                                    default:
                                    case 1: priority = 'low';       break;
                                    case 2: priority = 'normal';    break;
                                    case 3: priority = 'high';      break;
                                    case 4: priority = 'urgent';    break;
                                }

                                switch (currentTicket.status) {
                                    default:
                                    case 1: status = 'open';    break;
                                    case 2: status = 'pending'; break;
                                    case 3: status = 'hold';    break;
                                    case 4: status = 'solved';  break;
                                    case 5: status = 'closed';  break;
                                }

                                // Add conversation_id to ticket external_id
                                var currentTicket = {
                                    'external_id':  currentTicket.ticketId,
                                    'subject':      currentTicket.subject,
                                    'comments':     currentComments,
                                    'created_at':   currentTicket.created_at,
                                    'assignee_id':  assignee.id,
                                    'requester_id': requester.id,
                                    'group_id':     groupId,
                                    'tags':         ['show_ticket', 'migrated_ticket', `conversation_id_${currentTicket.ticketId}`],
                                    'priority':     priority,
                                    'status':       status,
                                    'custom_fields': [
                                        /*{
                                            'id':       ticketTypeField.id,
                                            'value':    currentTicket.message_type
                                        },*/
                                        {
                                            'id':       360010186999,
                                            'value':    "securechat"
                                        },
                                        {
                                            'id':       360010360380,
                                            'value':    "unpinned"
                                        },
                                        {
                                            'id':       360010371179,
                                            'value':    "read"
                                        },
                                        {
                                            'id':       360010480239,
                                            'value':    "archived"
                                        }
                                    ]
                                };

                            // Push each individual ticket into the batch array
                            ticketBatch.tickets.push(currentTicket);

                            } else { // data[x] is undefined
                                writeLog(`Row ${x} is undefined!`);
                                writeLog(`At: ${dir}/batched_${j}.log || Ticket ${x-batchTrack+1} of ${ticketsPerFile}`);
                                writeLog(`Source: ${file} || Row ${x}`);
                                console.log(`\nRow ${x} is undefined!`);
                                console.log(`At: ${dir}/batched_${j}.log || Ticket ${x-batchTrack+1} of ${ticketsPerFile}`);
                                console.log(`Source: ${file} || Row ${x}`);

                                var currentTicket = {};  
                            }
                        };
                        
                        // Write tickets to batchFile
                        batchFile.write(JSON.stringify(ticketBatch));
                        batchFile.end();

                        // Set start and end counters
                        batchTrack = batchEnd+1;
                        batchEnd = batchEnd+ticketsPerFile;

                        //writeLog(`Saved to file ${dir}/batched_${j}.log\n`);
                        //console.log(`Saved to file ${dir}/batched_${j}.log`);
                    };//for each batch file
                    console.log(`Finished ${dir} folder with ${batchEnd-ticketsPerFile+1} tickets\n`);
                    writeLog(`Finished ${dir} folder with ${batchEnd-ticketsPerFile+1} tickets\n`);
                //});//read batched tickets dir
            };//for each tickets file
            resolve();
        });//read tickets dir
    });
};//main

// Upload attachments and return tokens
async function uploadAttachment(ticketId, attachment) {
    return new Promise(async (resolve, reject) => {

        await zd.uploadFile(`attachments/${ticketId}/`, attachment)
        .then(res => {
            let newToken = res;
            writeLog(`Uploaded \'/attachments/${ticketId}/${attachment}\'`);
            writeLog(`with token ${newToken.token}`);
            resolve(newToken);
        })
        .catch(error => {
            let newToken = 'error';
            console.log('4', error);
            console.log(`Error encountered in attachment uploader for attachments/${ticketId}/${attachment}`);
            reject(newToken);
        });

        //let newToken = await zd.uploadFile(`attachments/${ticketId}/`, attachment).catch(error => console.log('4', error));
    });
}

// Toggle is intended to switch between pushing an uploaded token to tokens for public comments and the filename for internal comments
async function upload(currentAttachments, ticketId, x, c, tokens, fileNames, toggle) {
    return new Promise((resolve, reject)=> {
        if (Object.keys(currentAttachments).length !== 0) {
            fs.access(`attachments/${currentAttachments}`, async function(err) {
                if (err && err.code === 'ENOENT') {
                    resolve();
                } else {
                    var attachmentFolder = fs.readdirSync(`attachments/${currentAttachments}`);
                    if (attachmentFolder.length) { // If an attachment is found within the folder, upload or get the filename
                        if (toggle === 'public') {
                            writeLog(`\nTicket ${x}, Comment ${c}, ConvID ${ticketId} - Uploading ${attachmentFolder.length} attachment(s)`);
                            console.log(`\nTicket ${x}, Comment ${c}, ConvID ${ticketId} - Uploading ${attachmentFolder.length} attachment(s)`);
                            for(var a = 0; a < attachmentFolder.length; a++) {
                                var fileName = path.basename(attachmentFolder[a]);
                                console.log(`Uploading ${fileName}`);
                                writeUpload(`${x},${c},${ticketId},${a}: Uploading ${fileName}`);
                                
                                var token = {};
                                var tryCount=0;
                                
                                while ((token === 'error' && tryCount < 11) || tryCount === 0) {
                                    tryCount++;

                                    await uploadAttachment(currentAttachments, fileName)
                                    .then(res => {
                                        token = res;
                                        tokens.push(token.token);
                                        fileNames.push(fileName);
                                        writeUpload(`${x},${c},${ticketId},${a}: Success`);
                                        resolve();
                                    })
                                    .catch(error => {
                                        token = error;
                                        writeUpload(`${x},${c},${ticketId},${a}: Error`);
                                    });
                                }
                                
                                if (tryCount === 11) {
                                    console.log(`${x},${c},${ticketId},${a}: Skipping upload for attachments/${currentAttachments}/${fileName}`);
                                    writeUpload(`${x},${c},${ticketId},${a}: Skipping upload for attachments/${currentAttachments}/${fileName}`);
                                    writeUpload(`${x},${c},${ticketId},${a}: Re-try limit reached`);
                                    resolve();
                                }
                            }
                        } else if (toggle === 'internal'){
                            var fileName = path.basename(attachmentFolder[0]);
                            fileNames.push(fileName);
                            resolve();
                        }
                    }
                }
            });
        };
    });
}

// Search for a user by name, email, of cif/external_id within the array of user objects
function findUser(array, search) {
    return new Promise(function(resolve, reject){
        array.find(function(object, i){
            if (object.name === search || object.email === search || object.cif === search){
                resolve(object);
            }
        });
        // In case array.find doesn't resolve, use a default
        resolve({
            "id":       1, // Default ID
            "name":     `No User Found for ${search}`,
            "email":    `No User Found for ${search}`,
            "cif":      `No User Found for ${search}`
        });
    });
}


function getUsers() {
    return new Promise(function (resolve, reject){
        request({
            method: 'GET',
            url: `https://${subdomain}.zendesk.com/api/v2/users.json`
        }, async function (error, res, body) {
            var b = JSON.parse(body);
            var pages = Math.ceil(b.count/100);
            console.log(`${pages} of 100 users each found`);
            for (var i = 1; i <= pages; i++) {
                console.log(`Users: ${i}/${pages}`)
                await requestUsers(i);
            }
            resolve();
        }).auth(username, password);

    });
}

function requestUsers(p) {
    return new Promise(function (resolve,reject){
        request({
            method: 'GET',
            url: `https://${subdomain}.zendesk.com/api/v2/users.json?page=${p}`
        }, function (error, res, body) {
            var b = JSON.parse(body);
            for (var n=0;n<b.users.length;n++) {
                var extID = b.users[n].external_id;
                // For each user found in the request, populate the user object
                var user = {
                    "id":       b.users[n].id,
                    "name":     b.users[n].name.toLowerCase(),
                    "email":    b.users[n].email,
                    "cif":      extID != null ? extID.substring(0, extID.length - 3) : extID
                };
                users.push(user);
            }
            resolve();
        }).auth(username, password);
    })
}

// Get users from users.csv
async function usersFromCsv() {
    return new Promise(async (resolve, reject) => {
        console.log(`Getting users`)
        fs.createReadStream(`users\\users.csv`).pipe(csv()).on('data', async(row) => {
            users.push(row);
        }).on('end', async() => { // Fire 'end' event when all rows are processed
            resolve();
        });
    });
}

// Get users from users.csv
async function clearBatchedTickets() {
    return new Promise(async (resolve, reject) => {
        fs.readdir('batched_tickets', async(err, files) => {
            if(files.length === 0) {resolve();};
            if (err) throw err;
            console.log(`Clearing batched_tickets folder`);
            var count = 0;
            for (const f of files) {
                rimraf(`batched_tickets\\${f}`, function() {
                    console.log(`Deleted ${f}`);
                    count++;
                    if(count === files.length || files.length === 0) {resolve();};
                });
                
            };

        });
    });
}

// Get list of user IDs and names from Zendesk
// Used for mapping user names to Zendesk ID
function getUsersOLD(url) {
    return new Promise(function (resolve, reject){
        var u;
        if (url != undefined) {
            u = url; // Recursively uses the next_page object after a request
        } else {
            u = `https://${subdomain}.zendesk.com/api/v2/users.json`; // Url used in first request
        }

        request({
            method: 'GET',
            url: u
        }, function (error, res, body) {
            if (!error && (res.statusCode == 201 || res.statusCode == 200)) {
                var b = JSON.parse(body);
                for (var n=0;n<b.users.length;n++) {
                    var extID = b.users[n].external_id;
                    // For each user found in the request, populate the user object
                    var user = {
                        "id":       b.users[n].id,
                        "name":     b.users[n].name.toLowerCase(),
                        "email":    b.users[n].email,
                        "cif":      extID != null ? extID.substring(0, extID.length - 3) : extID
                    };
                    users.push(user);
                }

                if (res.headers['x-rate-limit-remaining'] < 20) { // Respect request limit
                    setTimeout(() => {
                        if (b.next_page != null) {
                            getUsers(b.next_page);
                        } else {
                            resolve(users);
                        }
                    }, 15000);
                }
                else
                    if (b.next_page != null) {
                        getUsers(b.next_page); // Recursively get the next page of users
                    } else {
                        resolve(users); // Return finished user array
                    }
            } else {
                console.log(error);
            }
        }).auth(username, password);
    });
}

// Search for a ticket field by title within the array of ticket field objects
function findField(array, search) {
    return new Promise(function(resolve, reject){
        array.find(function(object, i){
            if (object.title == search){
                resolve(object);
            }
        });
    });
}

// Get all ticket fields of the instance
function getFields() {
    return new Promise(function (resolve, reject){
        var u = `https://${subdomain}.zendesk.com/api/v2/ticket_fields.json`;
        var ticket_fields = [];
        request({
            method: 'GET',
            url: u
        }, function (error, res, body) {
            if (!error && (res.statusCode == 201 || res.statusCode == 200)) {
                var b = JSON.parse(body);
                for (var n=0;n<b.ticket_fields.length;n++) {
                    // For each ticket field found in the request, populate the ticket field object
                    var ticket_field = {
                        "id":       b.ticket_fields[n].id,
                        "title":    b.ticket_fields[n].title
                    };
                    ticket_fields.push(ticket_field);
                }

                if (res.headers['x-rate-limit-remaining'] < 20) { // Respect request limit
                    setTimeout(() => {
                        resolve(ticket_fields);
                    }, 15000);
                }
                else
                    resolve(ticket_fields);
            } else {
                console.log(error);
            }
        }).auth(username, password);
    });
}

function printProgress(msg) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(`${msg}`);
}

// Write to process log file with timestamp
async function writeLog(msg) {
    var t = await getToday();
    log.write(`${t}-${msg}\n`);
}

// Write to process log file with timestamp
async function writeUpload(msg) {
    var t = await getToday();
    uploadLog.write(`${t}-${msg}\n`);
}

// Initialise log files
function initLogFiles(date) {
    log = fs.createWriteStream(`log_files/${date}_2-MAP-TICKETS-UPLOAD-ATTACH-${subdomain}.log`, { flags: 'a' }); // Process log file
    uploadLog = fs.createWriteStream(`log_files/${date}_2-MAP-TICKETS-UPLOAD-ATTACH-${subdomain}_ATTACHMENTS.log`, { flags: 'a' }); // Process log file
}

// Get today's date and time in an appropriate format for logging purposes
function getToday() {
    return new Promise((resolve, reject) => {
        var today = new Date();
        var d = String(today.getDate()).padStart(2, '0');
        var m = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var y = today.getFullYear();
        var h = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
        var min = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
        var s = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();
        var mer = today.getHours() > 12 ? "PM" : "AM";
        
        today = `${y}-${m}-${d}_${h}-${min}-${s}${mer}`;

        resolve(today);
    });
}