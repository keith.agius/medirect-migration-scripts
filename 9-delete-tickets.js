'use strict';
const fs = require('fs');
const request = require("request");
const zd = require('./zendesk-lib');

// Set Zendesk credentials
const username = 'keith.agius@imovo.com.mt/token';
//const password = 'J44YNedG0GvZFP26PYGPjjxKleTpQLvOAa6QD5Wa'; // d3v-imovo token
const password = 'mwcMTvgaw6u2CeBL1VPi3efvO7SR5GGM1wUQcx0j'; // medirectbank1587974674 token
//const subdomain = 'd3v-imovo';
const subdomain = 'medirectbank1587974674';

var jobs = [], files = [], total = 0, progress = 0;

main();
async function main() {
    zd.setCredentials(username, password, subdomain);
    const dir = 'created_tickets';
    fs.readdir(dir, async(err, folders) => {
        total = folders.length;
        console.log(`${total} total file(s) found`);
        for (var i = 0; i < total; i++) {
            var filename = (`created_tickets/${folders[i]}`);
            var toDelete = fs.readFileSync(filename, 'utf8');
            toDelete = toDelete.substr(1); // Remove first square bracket
            toDelete = toDelete.slice(0, -1); // Remove last square bracket
            
            var delres = await deleteManyTickets(toDelete);
            delres = JSON.parse(delres);

            files.push(`created_tickets/${folders[i]}`);
            jobs.push(delres.job_status.id);
        }
        await checkJobs();
    });

    /*
    zd.setCredentials(username, password, subdomain);
    zd.DELETE_ALL_TICKETS(38068,38066); // BRUTE FORCE DELETE - Specify last ID and first ID to delete indiscriminantly
    */
}

function deleteManyTickets(ticketIds) {
    return new Promise(function(resolve, reject) {
        request({
            method: 'DELETE',
            uri: 'https://' + subdomain + '.zendesk.com/api/v2/tickets/destroy_many.json?ids=' + ticketIds
        }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                resolve(body);
            } else {
                setTimeout(() => {
                    reject(error);
                }, 10000);
            }
        }).auth(username, password);
    });
}


// Checks job and batch statuses
async function checkJobs() {
    return new Promise(async(resolve, reject) => {
        if (jobs.length <= 30) {
            let res = await zd.getManyJobsStatus(jobs).catch(error => console.log('5', error));
            res = JSON.parse(res);
            for (let x = 0; x < res.job_statuses.length; x++) {
                let job = res.job_statuses[x];
                console.log(`${job.id} - Checking`);
                if (job.status === 'completed') {
                    console.log(`${job.id} - Success - Deleted\n`);                    

                    // Remove succesful job ID from respective arrays and delete tickets file
                    const index = jobs.indexOf(job.id);
                    if (index > -1) {
                        fs.unlinkSync(`${files[index]}`)
                        files.splice(index, 1);
                        jobs.splice(index, 1);                        
                    }
                    progress++;

                } else if (job.status === 'failed') { // Failed jobs are stored in a separate log file so that they can be re-uploaded at a later stage
                    const index = jobs.indexOf(job.id);
                    console.log(`${job.id} - Fail)`);

                    // Remove failed job ID from respective arrays
                    if (index > -1) {
                        jobs.splice(index, 1);
                    }
                } else if (job.status === 'working') {
                    console.log(`${job.id} - ${job.status} - ${job.progress}/${job.total} - ${job.message}\n`);
                } else {
                    console.log(`${job.id} - ${job.status} - ${job.message}\n`);
                }
            };
            if (progress === total) {
                if (jobs.length === 0) {
                    console.log(`\nFinished\n`);
                    resolve();
                } else { // Entire process has finished and is currently waiting on jobs, so recursively checkjobs
                    checkJobs();
                }

            } else {
                resolve();
            }
        } else if (jobs.length > 30) { // Recursively check job statuses until space is freed
            printProgress(`30+ jobs queued, waiting`);
            setTimeout(async() => {
                await checkJobs();
            }, 5000);
        }
    });
}

function printProgress(msg) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(`${msg} \t\t_ ${progress}/${total}`);
}