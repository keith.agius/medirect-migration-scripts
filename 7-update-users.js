"use strict";

'use strict';
const fs = require('fs');
const rimraf = require('rimraf');
const request = require("request");
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

// Set Zendesk credentials
//const username = 'keith.agius@imovo.com.mt/token';
const username = 'opzendeskapi@medirect.com.mt/token';
//const password = 'J44YNedG0GvZFP26PYGPjjxKleTpQLvOAa6QD5Wa'; // d3v-imovo token
const password = 'M3K2F5wwIf5jZH08UhSsJ03kA68DfOVpptsA57iU'; // medirectbank1587974674 token
//const subdomain = 'd3v-imovo';
const subdomain =  'medirectbank1587974674';

var users = []

main();

async function main () {
    // Refresh users folder
    if (!fs.existsSync(`users`)) {
        fs.mkdirSync(`users`);
    }

    fs.readdir('users', async(err, files) => {
        if (err) throw err;
        console.log(`Clearing users folder`);
        for (const f of files) {
            rimraf(`users\\${f}`, function() { console.log(`Deleted ${f}`); });
        };
    });

    await getUsers();
    // Specify csv headers - 'id' must match those in requestUsers()
    const csvWriter = createCsvWriter({
        path: 'users/users.csv',
        header: [
          {id: 'id', title: 'id'},
          {id: 'name', title: 'name'},
          {id: 'email', title: 'email'},
          {id: 'cif', title: 'cif'},
        ]
      });

    csvWriter.writeRecords(users).then(()=> console.log('The CSV file was written successfully'));
}

function getUsers() {
    return new Promise(function (resolve, reject){
        request({
            method: 'GET',
            url: `https://${subdomain}.zendesk.com/api/v2/users.json`
        }, async function (error, res, body) {
            var b = JSON.parse(body);
            var pages = Math.ceil(b.count/100);
            console.log(pages);
            for (var i = 1; i <= pages; i++) {
                console.log(`${i}/${pages}`)
                await requestUsers(i);
            }
            resolve();
        }).auth(username, password);

    });
}

function requestUsers(p) {
    return new Promise(function (resolve,reject){
        request({
            method: 'GET',
            url: `https://${subdomain}.zendesk.com/api/v2/users.json?page=${p}`
        }, function (error, res, body) {
            var b = JSON.parse(body);
            for (var n=0;n<b.users.length;n++) {
                var extID = b.users[n].external_id;
                // For each user found in the request, populate the user object
                var user = {
                    "id":       b.users[n].id,
                    "name":     b.users[n].name.toLowerCase(),
                    "email":    b.users[n].email,
                    "cif":      extID != null ? extID.substring(0, extID.length - 3) : extID
                };
                users.push(user);
            }
            resolve();
        }).auth(username, password);
    })
}