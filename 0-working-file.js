"use strict";

const prepare = require('./1-prepare-tickets.js');
const map = require('./2-map-tickets-upload-attach.js');
const upload = require('./3-upload-tickets.js');

main();
async function main() {
    console.log(`\n\n******\n\n1-prepare-tickets.js STARTED\n\n******\n\n`);
    await prepare.prepareTickets();
    console.log(`\n\n******\n\n1-prepare-tickets.js FINISHED\n\n******`);
    console.log(`\n\n******\n\n2-tickets-upload-attach.js STARTED\n\n******\n\n`);
    await map.mapTickets();
    console.log(`\n\n******\n\n2-tickets-upload-attach.js FINISHED\n\n******`);
    console.log(`\n\n******\n\n3-upload-tickets.js STARTED\n\n******\n\n`);
    await upload.main();
    console.log(`\n\n******\n\n3-upload-tickets.js FINISHED\n\n******`);
};