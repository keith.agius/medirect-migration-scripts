const request = require("request");
const fs = require('fs');

let username = '';
let password = '';
let subdomain = '';

const zendesk = {
    getRequest, setCredentials, getManyJobsStatus, importTicket, importManyTickets, getJobStatus, uploadFile, DELETE_ALL_TICKETS
}

function getRequest(url) {
    return new Promise(function (resolve, reject) {
        var options = {
            url: 'https://' + subdomain + '.zendesk.com' + url,
            forever: true
        }
        
        request(options, function (error, res, body) {
            if (!error && res.statusCode == 200) {
                if (res.headers['x-rate-limit-remaining'] < 5) {
                    log.write("Waiting 15 seconds...\n");
                    setTimeout(() => {
                        resolve(body)
                    }, 15000);
                }
                else
                    resolve(body);
            } else {
                console.log('ERROR', error);
                reject(error);
            }
        }).auth(username, password);
    });
}

function setCredentials(u, p, s) {
    username = u;
    password = p;
    subdomain = s;
};

function importTicket(data) {
    return new Promise(function (resolve, reject) {
        var options = {
            method: 'POST',
            url: 'https://' + subdomain + '.zendesk.com/api/v2/imports/tickets.json',
            headers: { 'Content-Type': 'application/json' },
            json: data,
            forever: true
        };
        request(options, function (error, res, body) {
            if (!error && (res.statusCode == 201 || res.statusCode == 200)) {
                if (res.headers['x-rate-limit-remaining'] < 20) {
                    setTimeout(() => {
                        resolve(body.ticket);
                    }, 15000);
                }
                else
                    resolve(body.ticket);
            } else {
                reject(error);
            }
        }).auth(username, password);
    });
}
/* Expecting data in the format:
{
  "tickets": [
    {
      "requester_id": 827,
      "assignee_id": 19,
      "subject": "Some subject",
      "description": "A description",
      "tags": [ "foo", "bar" ],
      "comments": [
        { "author_id": 827, "value": "This is a comment", "created_at": "2009-06-25T10:15:18Z" },
        { "author_id": 19, "value": "This is a private comment", "public": false }
      ]
    },
    {
      "requester_id": 830,
      "assignee_id": 21,
      "subject": "Some subject",
      "description": "A description",
      "tags": [ "foo", "bar" ],
      "comments": [
        { "author_id": 830, "value": "This is a comment", "created_at": "2009-06-25T10:15:18Z" },
        { "author_id": 21, "value": "This is a private comment", "public": false }
      ]
    }
  ]
}
*/
function importManyTickets(data) {
    return new Promise(function (resolve, reject) {
        var options = {
            method: 'POST',
            url: 'https://' + subdomain + '.zendesk.com/api/v2/imports/tickets/create_many.json',
            headers: { 'Content-Type': 'application/json' },
            json: data,
            forever: true
        };
        request(options, function (error, res, body) {
            if (!error && (res.statusCode == 201 || res.statusCode == 200)) {
                if (res.headers['x-rate-limit-remaining'] < 20) {
                    setTimeout(() => {
                        resolve(body);
                    }, 15000);
                }
                else
                    resolve(body);
            } else {
                reject(error);
            }
        }).auth(username, password);
    });
}

function getJobStatus(jobId) {
    return new Promise(function (resolve, reject) {
        var options = {
            url: 'https://' + subdomain + '.zendesk.com/api/v2/job_statuses/' + jobId + '.json',
            forever: true
        }
        request(options, function (error, res, body) {
                if (!error && res.statusCode == 200) {
                    if (res.headers['x-rate-limit-remaining'] < 15) {
                        setTimeout(() => {
                            resolve(body);
                        }, 15000);
                    }
                    else
                        resolve(body);
                } else {
                    reject(error);
                }
            }).auth(username, password);
    });
}

function getManyJobsStatus(jobIds) {
    return new Promise(function (resolve, reject) {
        var options = {
            url: 'https://' + subdomain + '.zendesk.com/api/v2/job_statuses/show_many.json?ids=' + jobIds.toString(),
            forever: true
        }
        request(options, function (error, res, body) {
                if (!error && res.statusCode == 200) {
                    if (res.headers['x-rate-limit-remaining'] < 15) {
                        setTimeout(() => {
                            resolve(body);
                        }, 15000);
                    }
                    else
                        resolve(body);
                } else {
                    reject(error, res, body);
                }
            }).auth(username, password);
    });
}

/*
    pathToFolder should be './......./
    filename is including the extension
*/
function uploadFile(pathToFolder, filename) {
    return new Promise(function (resolve, reject) {
        let readStream;
        try {
            readStream = fs.createReadStream(pathToFolder + filename);
            readStream.on('error', (error) => { console.log("Caught", error); reject(); });
        } catch {
            reject();
        }
        request({
            url: 'https://' + subdomain + '.zendesk.com/api/v2/uploads.json?filename=' + filename,
            method: 'POST',
            headers: {
                'content-type': 'application/binary'
            },
            encoding: null,
            body: readStream,
            forever: true
        }, function (error, res, body) {
            if (!error && res.statusCode == 201) {
                if (res.headers['x-rate-limit-remaining'] < 20) {
                    setTimeout(() => {
                        resolve(JSON.parse(body).upload);
                    }, 15000);
                }
                else
                    resolve(JSON.parse(body).upload);
            } else {
                reject(error, res, body);
            }
        }).auth(username, password);
    });
}

async function DELETE_ALL_TICKETS(lastId, startId) {
    t = [];
    for (let x = lastId; x > startId; x--) {
        t.push(x);
        if (t.length === 100) {
            while (true) {
                try {
                    await deleteManyTickets(t);
                    t = [];
                    break;
                } catch (e) {
                }
            }
        }
    }
    while (true) {
        try {
            await deleteManyTickets(t);
            t = [];
            break;
        } catch (e) {
        }
    }
}

function deleteTicket(ticketId) {
    return new Promise(function (resolve, reject) {
        request({
            method: 'DELETE',
            uri: 'https://' + subdomain + '.zendesk.com/api/v2/tickets/' + ticketId + '.json',
            forever: true
        }, function (error, res, body) {
            if (!error && res.statusCode == 204) {
                if (res.headers['x-rate-limit-remaining'] < 20) {
                    writeLog("Waiting 15 seconds...");
                    setTimeout(() => {
                        resolve(body)
                    }, 15000);
                }
                else
                    resolve(body);
            } else {
                //   reject(error);
                console.log('amen');
            }
        }).auth(username, password);
    });
}
function deleteManyTickets(ticketIds) {
    return new Promise(function (resolve, reject) {
        request({
            method: 'DELETE',
            uri: 'https://' + subdomain + '.zendesk.com/api/v2/tickets/destroy_many.json?ids=' + ticketIds.join(),
            forever: true
        }, function (error, res, body) {
            if (!error && res.statusCode == 200) {
                resolve(body);
            } else {
                setTimeout(() => {
                    reject(error);
                }, 10000);
            }
        }).auth(username, password);
    });
}

module.exports = zendesk;